<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;

use Acme\Facades\Activity;

use App\Brand;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($name = $request->name) {
            $data = Brand::where('name', 'LIKE', '%' . $name . '%')->paginate(25);
        } else {
            $data = Brand::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/brands/index')
            ->with('title', 'Brands')
            ->with('menu', 'brands')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/brands/create')
            ->with('title', 'Create brand')
            ->with('menu', 'brands');
    }
    
    public function store(BrandRequest $request)
    {
        $input = $request->all();
        $brand = Brand::create($input);
        
        // $log = 'creates a new brand "' . $brand->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminBrandsEdit', [$brand->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/brands/show')
            ->with('title', 'Show brand')
            ->with('data', Brand::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/brands/view')
            ->with('title', 'View brand')
            ->with('menu', 'brands')
            ->with('data', Brand::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Brand::findOrFail($id);

        return view('admin/brands/edit')
            ->with('title', 'Edit brand')
            ->with('menu', 'brands')
            ->with('data', $data);
    }
    
    public function update(BrandRequest $request, $id)
    {
        $input = $request->all();
        $brand = Brand::findOrFail($id);
        $brand->update($input);

        // $log = 'edits a brand "' . $brand->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Brand::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->name;
        }
        // $log = 'deletes a new brand "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Brand::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminBrands')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/brands', array('as'=>'adminBrands','uses'=>'Admin\BrandController@index'));
Route::get('admin/brands/create', array('as'=>'adminBrandsCreate','uses'=>'Admin\BrandController@create'));
Route::post('admin/brands/', array('as'=>'adminBrandsStore','uses'=>'Admin\BrandController@store'));
Route::get('admin/brands/{id}/show', array('as'=>'adminBrandsShow','uses'=>'Admin\BrandController@show'));
Route::get('admin/brands/{id}/view', array('as'=>'adminBrandsView','uses'=>'Admin\BrandController@view'));
Route::get('admin/brands/{id}/edit', array('as'=>'adminBrandsEdit','uses'=>'Admin\BrandController@edit'));
Route::patch('admin/brands/{id}', array('as'=>'adminBrandsUpdate','uses'=>'Admin\BrandController@update'));
Route::delete('admin/brands/destroy', array('as'=>'adminBrandsDestroy','uses'=>'Admin\BrandController@destroy'));
*/
}
