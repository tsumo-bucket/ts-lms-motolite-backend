<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Brand extends BaseModel
{
    
    protected $fillable = [
    	'name',

        'logo',

        'background_image',

        'published',
    	];
}
