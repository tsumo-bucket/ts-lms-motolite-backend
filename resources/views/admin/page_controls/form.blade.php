<div class="caboodle-form-group">
  <label for="reference_id">Reference Id</label>
  {!! Form::text('reference_id', null, ['class'=>'form-control', 'id'=>'reference_id', 'placeholder'=>'Reference Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="reference_type">Reference Type</label>
  {!! Form::text('reference_type', null, ['class'=>'form-control', 'id'=>'reference_type', 'placeholder'=>'Reference Type', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="label">Label</label>
  {!! Form::text('label', null, ['class'=>'form-control', 'id'=>'label', 'placeholder'=>'Label', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="type">Type</label>
  {!! Form::select('type', ['text' => 'text', 'number' => 'number', 'checkbox' => 'checkbox', 'textarea' => 'textarea', 'asset' => 'asset', 'select' => 'select', 'color' => 'color', 'date' => 'date', 'time' => 'time', 'date_time' => 'date_time', 'products' => 'products', 'tags' => 'tags'], null, ['class'=>'form-control select2']) !!}
</div>
<div class="caboodle-form-group">
  <label for="options_json">Options Json</label>
  {!! Form::textarea('options_json', null, ['class'=>'form-control redactor', 'id'=>'options_json', 'placeholder'=>'Options Json', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="caboodle-form-group">
  <label for="required">Required</label>
  <br />
   <?php $boolCheck = (@$data->required) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('required', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('required', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'required','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="caboodle-form-group">
  <label for="value">Value</label>
  {!! Form::textarea('value', null, ['class'=>'form-control redactor', 'id'=>'value', 'placeholder'=>'Value', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
