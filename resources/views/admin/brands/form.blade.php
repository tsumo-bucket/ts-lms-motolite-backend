<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group sumo-asset-select image-banner">
  <label for="logo">Logo</label>
  {!! Form::hidden('logo', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id]) !!}
  <span class="sub-text-1">The required logo size is 300x300 pixels minimum</span>
</div>
<div class="caboodle-form-group sumo-asset-select image-banner">
  <label for="background_image">Background Image</label>
  {!! Form::hidden('background_image', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id]) !!}
  <span class="sub-text-1">The required background image size is 1200x600 pixels minimum</span>
</div>
<div class="caboodle-form-group">
  <label for="published">Published</label>
  {!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
</div>
