<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="description">Description</label>
  {!! Form::text('description', null, ['class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description']) !!}
</div>
{!! Form::hidden('parent', $parent_id) !!}